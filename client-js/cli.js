#!/usr/bin/env node
var request   = require('request');
var fs        = require('fs')
var R         = require('ramda');
// https://www.npmjs.com/package/yargs | http://yargs.js.org/
var argv      = require('yargs').argv;
var jwtDecode = require('jwt-decode');
var bearer    = argv.bearer ? argv.bearer : "eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCJ9.eyJleHAiOiAxNTI3MDY3NzUwLCAic2NvcGVzIjogeyIvYXBpL2FsaWFzL2lzYWFjdXN0ZXN0IjogWyJQT1NUIiwgIkdFVCJdfSwgInN1YiI6ICJpc2FhY3VzdGVzdCIsICJhdWQiOiAicHNldWRvLmZpbW0uZmkiLCAibmJmIjogMTQ5NTUzMTc1MCwgImlhdCI6IDE0OTU1MzE3NTAsICJqdGkiOiAienRnY0tqeDlwb1ZEM28xc0hFeVB3QT09In0.ROC672l_q9br03mezNbsIYaNFoI5yCcRX1mS56NsPck"
// decoded JWT claims
var decoded   = jwtDecode(bearer)

// JWT audience
var svc = argv.svc ? argv.svc : ('https://'+decoded['aud'])

// Pseudonymization domain or scope.
var scope = argv.scope ? argv.scope : ( decoded['sub'] )

if(argv.debug){
    console.log("sub", decoded.sub);
    console.log("svc", svc);
    console.log("scope",scope)
    console.log("aud", decoded['aud']);
    console.log("claims", decoded);
    console.log( svc +'/api/alias/'+scope );
    console.log("input file", argv.input );
    console.log("output file", argv.output );
};

// https://github.com/request/request

var identifiers = []

if (argv.input ){
    fs.readFileSync(argv.input).toString().split(/\r?\n/).forEach(function(line){
      if (! line == ""){
          identifiers.push( line );
      }
    });
};

var options = {
 'url' : svc +'/api/alias/'+scope,
 'body': identifiers,
 'json': true,
 'headers' : {
     'Content-Type' : 'application/json',
     'charset': 'utf-8',
     'Authorization': 'Bearer '+bearer
  }
};

function callback(error, response, body){
    if (! error && response.statusCode == 200){
       // R.forEach((x) => console.log(x.id+'\t'+x.alias+'\t'+x.project) , body);
       if (argv.output ){
          fs.appendFileSync(argv.output, "ID" +'\t'+"ALIAS"+'\n')
          R.forEach( (x) => fs.appendFileSync(argv.output, x.id +'\t'+x.alias+'\n'), body);
       } else {
         console.log('ID\tALIAS');
         R.forEach((x) => console.log(x.id+'\t'+x.alias) , body);
       }
    }
    if (error || response.statusCode != 200 ) {
        console.log("error:", error);
        console.log(response.statusCode);
    };
    if (response.statusCode == 401){
        console.log("unauthorized", response.body);
    };
};

if (argv.input){
    request.post(options, callback);
} else {
    console.log('--input required')
};
