import hmac
import hashlib
from hashlib import sha256
from hashlib import sha1
from hashlib import sha512
import string
"""

fast hashing functions
https://cyan4973.github.io/xxHash/

"""
#class PseudoFun:
digest_hmac = lambda key, msg: int(hmac.new(str.encode(key), str.encode(msg), sha256).hexdigest(), 16 )

digest_hmac_sha1 = lambda key, msg: int(hmac.new(str.encode(key), str.encode(msg), hashlib.sha1).hexdigest(), 16 )

digest_hmac_siphash24 = lambda key, msg: hash(int(hmac.new(str.encode(key), str.encode(msg), hashlib.sha1).hexdigest(), 16 ) )

digest_hmac_siphash24_32 = lambda key, msg: digest_hmac_siphash24(key, msg) & 0xffffffff

digest_hmac_sha512 = lambda key, msg: int(hmac.new(str.encode(key), str.encode(msg), hashlib.sha512).hexdigest(), 16 )


ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

JWT_ALGORITHM = 'HS256'

#ALPHABET_32 = string.digits + string.ascii_uppercase

BASE36 = string.digits + string.ascii_uppercase

