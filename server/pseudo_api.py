#!/usr/bin/python3
#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
from bottle     import Bottle, run, request, response, abort
from pymongo    import MongoClient
#import mmh3
#import pystache
import hmac
import hashlib

from hashlib import sha256
from hashlib import sha1
from hashlib import sha512

from simplejson import dumps

import bottle
import string
import random

import python_jwt as jwt
import datetime
import re
import base64
import json

#from pseudolib import digest_hmac_siphash24
import pseudolib
import pseudo_api_auth

#from pseudolib import basex_encode, basex_decode
from pseudo_api_auth import parse_jwt_token, jwtauth

import configparser

from bases import Bases

''' bases.py instance
    see: https://github.com/belldandu/bases.py
'''
bases = Bases()

''' base-X encoder (Default: Base32)
'''
encoder = lambda  num,algo=bases.toBase32: algo( num )[:10]

''' Bottle app
'''
app = Bottle()
mongo = MongoClient("localhost", 27017)

''' 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
'''
ALPHABET = string.digits + string.ascii_lowercase + string.ascii_uppercase

BASE36 = string.digits + string.ascii_uppercase

JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_MINUTES = '60'

''' Default JWT algorithm HMAC SHA256
'''
DEFAULT_HMAC_ALGO  = pseudolib.digest_hmac_sha1
DEFAULT_SHORT_HASH = pseudolib.digest_hmac_siphash24

def configure():
    global config
    config = configparser.ConfigParser()
    config.read('app.conf')

def key_generator(size=32, chars = ALPHABET):
    return ''.join(random.choice(chars) for _ in range(size))

def get_secret(scope):
    Keys = mongo['isaacus']['Keys']
    if Keys.find_one( {"_id": scope} ) == None:
        secret = key_generator()
        Keys.insert( {
            "_id": scope,
            "key": secret,
            "scope": scope,
            "encoding": "Base32",
            "hash": "HMAC-SHA1",
            "short-hash": "Siphash24"
        })
    else:
        secret = Keys.find_one( {"_id": scope} )["key"]
    return secret

def convert_identifiers(identifiers, scope, algo = DEFAULT_SHORT_HASH):
    HMAC_SECRET = get_secret(scope)
    li = []
    for _id in identifiers:
        longid = str( DEFAULT_HMAC_ALGO(HMAC_SECRET, _id) )
        alias = mkalias( _id, longid, scope, HMAC_SECRET, algo )
        alias_doc = {"id": _id, "alias": alias, "project": scope}
        li.append( alias_doc )
    return li

def mkalias(_id, longid, scope, secret, algo = DEFAULT_SHORT_HASH):
    Aliases = mongo['isaacus']['Alias']
    ''' Create a new pseudonyme (alias) using the given hash algorithm
    '''
    alias = algo(secret, _id)
    current = Aliases.find_one( {"alias": encoder(alias), "project": scope } )
    if current == None:
        doc = {"_id": longid, "alias":  encoder(alias), "project": scope  }
        Aliases.insert(doc)
        return doc['alias']
    else:
        if current["_id"] == longid:
            return current['alias']
        else:
            print("collision!\t{} ".format(_id))
            return mkalias( encoder(alias), longid, scope, secret, algo )

@app.route('/')
def index():
    return "Isaacus pseudonymization API"

@app.route('/api/alias/<scope>', method='POST')
@jwtauth()
def alias_short(scope):
    li = []
    try:
        data = request.json
        if data:
            secret = get_secret(scope)
            li = convert_identifiers(data, scope, pseudolib.digest_hmac_siphash24)

    except:
        raise ValueError
        print(e)

    response.content_type = "application/json"
    response.set_header("charset","utf-8")
    return dumps(li)

"""
@app.route('/login', method='POST')
def jwtlogin():
    key = "YBPNGX132UVL4TI5IEXMZ3M9LUZ4CRPG"
    payload = {'foo': 'bar', "scopes":{"projects":["123","321"]}}
    token = jwt.generate_jwt(payload, key, JWT_ALGORITHM, datetime.timedelta(minutes=60))
    header, claims = jwt.verify_jwt(token, key, [JWT_ALGORITHM])
    ## debugging
    print("token type: {}".format(type(token)))
    print(claims)
    print(token)
    ##
    response.content_type = "application/json"
    response.set_header("charset","utf-8")
    return dumps({"token": token}, indent=2)
"""

@app.route('/test/<scope>', method="POST")
@jwtauth()
def test( scope, *args, **kwargs):
    print(scope)
    return "KO"


# run app in development mode
if __name__ == '__main__':
    configure()
    confix = configparser.ConfigParser()
    confix.read('app.conf')
    print("starting pseudo api...")
    print( confix.sections() )
    print("global.. {}".format(  config.sections() )   )

    bottle.run(app, host='localhost', port=8082, reloader=True)

# doesn't' work with gunicorn..
# run in application mode
#else:
#    app = application = bottle.default_app()

