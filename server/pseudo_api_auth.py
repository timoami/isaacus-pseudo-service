# -*- coding: UTF-8 -*-

import re
import json
import base64
import python_jwt as jwt
from bottle     import Bottle, run, request, response, abort
import configparser
import re
bearer_search = lambda auth_header: re.search("(?<=Bearer)\s{1,}\S{1,}$", auth_header)

JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_MINUTES = '60'
config = None

def configure():
    global config
    config = configparser.ConfigParser()
    config.read('app.conf')
    return config

def parse_jwt_token(auth_header):
    if  bearer_search( auth_header ):

        token = bearer_search( auth_header ).group(0).strip(" ")

        claims_part = token.split(".")[1]

        padded = str(claims_part) + '=' * (4 - len(str(claims_part)) % 4 )

        #claims = json.loads( base64.urlsafe_b64decode( claims_part + '===').decode('utf-8'))

        claims = json.loads( base64.urlsafe_b64decode( padded ).decode('utf-8') )

        return claims, token
    else:
        return None, None

def jwtauth():

    ''' Function for checking if the accessed resource matches allowed scopes
    '''
    scope_check = lambda key, actions, req: ( re.match(key, req.path) != None ) and ( actions.count(req.method) >= 1 )

    def get_jwt_key():
        global config
        if not config:
            configure()
        # get user's global key      
        appsecret = config["api"].get("secret") if config["api"].get("secret")  else "YBPNGX132UVL4TI5IEXMZ3M9LUZ4CRPG"

        return appsecret

    def authz(fun_action):
        def validate_action(*args, **kwargs):
            auth_header = request.headers.get("Authorization")
            
            ''' TODO: check audience vs request.urlparts.netloc 
            '''
            host = request.urlparts.netloc

            if auth_header:
                # validate jwt token
                resource_id = request.url_args.get("scope")

                if not resource_id:
                    resource_id = "admin"

                if bearer_search( auth_header ) and resource_id:

                    claims, token = parse_jwt_token(auth_header)

                    if claims == None or token == None:
                        """
                            ERROR! Token parsing failure (because of base64 padding issues)
                        """
                        return abort(401, "Error. Token parsing failure.")

                    """
                        Verify the JWT token and claims
                    """                  

                    try:
                        key = get_jwt_key()

                        jwt_header, jwt_claims = jwt.verify_jwt(token, key, [JWT_ALGORITHM])
                        
                        if jwt_claims.get("scopes") == None:
                            return abort(401, "Error. scopes not defined")

                        acl_ok = [  scope_check(x, jwt_claims['scopes'][x], request) for x in jwt_claims['scopes'].keys() ].count(True) >=1
 
                        if acl_ok:
                            return fun_action(*args, **kwargs)

                        else:
                            """
                                Not allowed to access resource.
                                resource_id was not listed in the signed claims.scopes
                            """
                            return abort(401,"Not allowed to access resource: {}".format(resource_id))

                    except Exception as e:
                        print("Error in parsing")
                        print("{}\n".format(e))
                        return abort(401, "Access denied. {}".format(e))
                else:
                    return abort(401, "Access denied. Invalid or no token")
            else:
                return abort(401, "Access denied. No token found")
        return validate_action
    return authz

