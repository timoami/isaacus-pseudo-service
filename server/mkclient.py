#!/usr/bin/python3
#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import hmac
#import mmh3
import hashlib

from hashlib import sha256
from hashlib import sha1
from hashlib import sha512

import string
import random

import argparse

from bases import Bases
import configparser
import python_jwt as jwt
import datetime
from simplejson import dumps

config = None

JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_MINUTES = '60'

def configure():
    global config
    config = configparser.ConfigParser()
    config.read('app.conf')
    return config

def get_jwt_key():
    global config
    
    if not config:
        configure()
    # get user's global key
    print("got config in jwtauth.. {}".format( config ))
        
    appsecret = config["api"].get("secret") if config["api"].get("secret")  else "YBPNGX132UVL4TI5IEXMZ3M9LUZ4CRPG"

    return appsecret

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--aud", required=False, default="pseudo.fimm.fi")
    parser.add_argument("--sub", required=False, default="1234567890")
    parser.add_argument("--resource", required=False, default="*")
    parser.add_argument("--validdays", required=False,type=int,  default=30)
    parser.add_argument("--secret", required=False, default=get_jwt_key())
    args = parser.parse_args()
    _collision_check = {}
    scopes = {'/api/alias/{}'.format(args.resource): ['POST','GET']}
    key = args.secret # get_jwt_key()
    print(key)
    payload = {
     'aud': args.aud,
     'sub': args.sub,
     'scopes': scopes
    }
    token = jwt.generate_jwt(payload, key, JWT_ALGORITHM, datetime.timedelta(days=args.validdays))
    header, claims = jwt.verify_jwt(token, key, [JWT_ALGORITHM])

    
    print( dumps( claims, indent=2, sort_keys=True ))
    print( token )


if __name__ == '__main__':
    main()
