#!/usr/bin/python3
#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import argparse
import csv
import requests

from time import sleep

import base64
import json
import re
import sys

_CHUNK_SIZE = 100

def decode_claims(token):
    try:
        claims_part = token.split(".")[1]
        return json.loads( base64.b64decode( claims_part + '=' *(-len( claims_part )%4)).decode('utf-8')  )
    except:
        return None

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", required=False)
    parser.add_argument("--scope", required=False)
    parser.add_argument("--input", required=True)
    parser.add_argument("--output", required=False)
    parser.add_argument("--bearer", type=str, required=True)

    args = parser.parse_args()

    headers = {'Content-Type': 'application/json', 'charset': 'utf-8', "Authorization" : "Bearer {}".format(args.bearer) }

    claims = decode_claims(args.bearer)

    ''' Pseudonymization service base address e.g. protocol://host
    '''
    svc_base = None
    scope = None

    ''' Determine the 'scope' (JWT subject) of the call
    '''
    if claims and not args.scope:
        scope = claims.get('sub')
    else:
        if args.scope:
            scope = args.scope
        else:
            sys.exit("Error. Missing valid subject 'sub' field in the token and/or --scope argument")

    ''' Determine the audience (aud) of the API call from token.
    '''
    if ( claims.get('aud') if claims else None ):
        audience = claims.get('aud')
        svc_base = audience if ( re.match("^http[s]{0,1}://.*", audience )) else 'https://'+audience

    else:
        if args.host:
            svc_base = args.host if ( re.match("^http[s]{0,1}://.*", args.host )) else 'https://'+args.host

        else:
            sys.exit("Error. Missing --host parameter or aud field in the token.")

    svc_url = "{}/api/alias/{}".format(svc_base, scope)

    ssn_list = []

    ''' read identifiers from (args.input) into a list (ssn_list)
    '''
    with open(args.input) as inf:
        for line in inf:
            ssn_list.append( line.strip("\n") )

    ''' split ssn_list into chunks
    '''
    chunks = [ssn_list[i:i + _CHUNK_SIZE] for i in range(0, len(ssn_list), _CHUNK_SIZE)]

    print("\t".join(["ID", "ALIAS", "PROJECT_ID"]))

    for chunk in chunks:
        resp = requests.post(svc_url, headers = headers, json=chunk)
        if (resp.status_code == 200):
            js = resp.json()
            if js:
                for doc in js:
                    print("\t".join([ doc['id'], doc['alias'], scope] ))

        else:
            print("Error {}".format(resp.status_code))
            print(resp.text)
            break

if __name__ == '__main__':
    main()

